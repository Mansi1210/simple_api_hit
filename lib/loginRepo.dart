import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:simple_api_hit/loginRequest.dart';
import 'package:simple_api_hit/model.dart';

class LoginRepo {
  int statusCode = 0;
  String message;

  Future<LoginRegisterResponse> tryLogin(LoginRequest request) async {
    final response = await http.post(
        Uri.parse('http://handymanbeta.quventix.com/web/handyman-user/login')
    );
        // body: request.toMap());
    // final responseData =request.toMap();
    // print(responseData);
    //
    // if (response.statusCode == 200) {
    //   print(response.body);
    //   return LoginRegisterResponse.fromJson(jsonDecode(response.body));
    // } else {
    //   print(" giving error");
    // }
    return LoginRegisterResponse.fromJson(json.decode(response.body));
  }
}