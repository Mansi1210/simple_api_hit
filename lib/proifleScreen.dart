
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          SizedBox(height: screenSize.height / 35),
          topBar(),
          SingleChildScrollView(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(38, 180, 30, 0),
                  child: middleContainer(context),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(110, 100, 30, 90),
                  child: profileIcon(),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget topBar() {
    return Container(
      width: double.infinity,
      height: 85,
      color: Colors.black,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
          ),
          Text(
            "Profile",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(
              Icons.edit,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  Widget middleContainer(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Container(
      width: screen.width / 0.9,
      height: screen.height / 1.3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      child: Column(
        children: [
          iconBox(context),
          line(),
          emailContainer(),
          line(),
          phoneContainer(),
          line(),
          doc()
        ],
      ),
    );
  }

  Widget iconContainer(BuildContext context) {
    var screen = MediaQuery.of(context).size;
    return Container(
      width: screen.width / 1,
      height: screen.height / 10,
    );
  }

  Widget iconBox(BuildContext context) {
    var screening = MediaQuery.of(context).size;
    return Container(
      width: screening.width / 1,
      height: screening.height / 9.5,
      //color: Colors.orangeAccent,
    );
  }

  Widget profileIcon() {
    return Container(
      height: 160,
      width: 150,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: new DecorationImage(
          image: ExactAssetImage('icon.jpeg'),
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }

  Widget line() {
    return Container(
      width: double.infinity,
      height: 1,
      color: Colors.grey,
    );
  }

  Widget emailContainer() {
    return Container(
      height: 180,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 130, 0, 10),
        child: Text("E-Mail Address",style: TextStyle(fontSize: 15)),
      ),
//  color: Colors.pink,
    );
  }
}

Widget phoneContainer(){
  return Container(
    height: 80,
    width: double.infinity,
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 30),
      child: Text("Mobile Number",style: TextStyle(fontSize: 15),),
    ),
    //color: Colors.orangeAccent,
  );
}

Widget doc(){
  return Container(
    alignment: Alignment.centerLeft,
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 50),
      child: Text("Documents",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,),),
    ),
  );
}
