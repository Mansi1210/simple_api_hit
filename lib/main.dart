// import 'dart:convert';
// import 'package:flash/flash.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_country_picker/flutter_country_picker.dart';
// import 'package:http/http.dart' as http;
// // import 'package:simple_api_hit/ApiService.dart';
// // import 'package:simple_api_hit/loginBloc.dart';
// //import 'package:simple_api_hit/loginRequest.dart';
// import 'package:simple_api_hit/model.dart';
// //import 'package:simple_api_hit/proifleScreen.dart';
//
// void main() {
//   runApp(MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(fontFamily: 'Gothic'),
//       home: MyHomePage(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }
//
// class MyHomePage extends StatefulWidget {
//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }
//
// class _MyHomePageState extends State<MyHomePage> {
//   Future<LoginRegisterResponse> futureAlbum;
//
//   // @override
//   // void initState() {
//    //
//   // }
//
//   var countryCode;
//   var _phoneController = TextEditingController();
//   var _psswrdController = TextEditingController();
//
//   FocusNode phone = FocusNode();
//   FocusNode pass = FocusNode();
//
//   bool obscureText = true;
//   IconData eye = Icons.visibility_off;
//
//   onClickLogin() async {
//     String phoneNo = _phoneController.text;
//     String psswrd = _psswrdController.text;
//
//     phone.requestFocus();
//     pass.requestFocus();
//     if (phoneNo.isEmpty) {
//       print("phone number can't be null");
//       _showCenterFlash(
//           position: FlashPosition.bottom,
//           style: FlashStyle.floating,
//           msg: 'phone number field cannot be empty');
//       phone.requestFocus();
//     } else if (phoneNo.length != 10) {
//       print("phone number can't be null");
//       _showCenterFlash(
//           position: FlashPosition.bottom,
//           style: FlashStyle.floating,
//           msg: 'phone number is not valid');
//       _phoneController.clear();
//       phone.requestFocus();
//     } else if (psswrd.isEmpty) {
//       print("password field can't be empty");
//       _showCenterFlash(
//           position: FlashPosition.bottom,
//           style: FlashStyle.floating,
//           msg: 'password field cannot be empty');
//       pass.requestFocus();
//     } else if (psswrd.length < 6) {
//       _showCenterFlash(
//           position: FlashPosition.bottom,
//           style: FlashStyle.floating,
//           msg: 'password is not valid ');
//       _psswrdController.clear();
//       pass.requestFocus();
//       print("password is not Valid");
//     } else {
//       // closeKyboard();
//       // Navigator.push(
//       //     context, MaterialPageRoute(builder: (context) => Profile()));
//       // LoginRequest requestBody = LoginRequest(
//       //     phoneNo: phoneNo,
//       //     password: psswrd,
//       //     device_token:
//       //         "fQJprSCMQVeYQDrA9FcYJE:APA91bEuHUm8sh1lfu9nFSadY6pxQtLy7Ge9hjm2kNvGXak4ZOvwH8_JrC0fx2wbqaojQeCAUF0ouluhcdQGIMT5syV3VQTkGku_Ok4yh3kQ_0pihBAMdQOtfkZC3Dcsf15tL4i4Ax6q",
//       //     country_code: '+91');
//       Map<String, String> body = {
//         "phone_number": phoneNo,
//         "password": psswrd,
//         "device_token":
//         "fQJprSCMQVeYQDrA9FcYJE:APA91bEuHUm8sh1lfu9nFSadY6pxQtLy7Ge9hjm2kNvGXak4ZOvwH8_JrC0fx2wbqaojQeCAUF0ouluhcdQGIMT5syV3VQTkGku_Ok4yh3kQ_0pihBAMdQOtfkZC3Dcsf15tL4i4Ax6q",
//         "country_code": "+91",
//         "loginToken": "e639c68cb28da92674c0efc0561614a9",
//         "login_type": '0',
//         "login_or_signup": '0',
//       };
//      Future<LoginRegisterResponse> createAlbum() async {
//         final  response = await http.post(
//           Uri.parse('http://handymanbeta.quventix.com/web/handyman-user/login'),
//           headers: <String, String>{
//             'Content-Type': 'application/json; charset=UTF-8',
//           },
//           body:json.encode(body)
//         );
//
//
//         if (response.statusCode == 201) {
//           return LoginRegisterResponse.fromJson(json.decode(response.body));
//         } else {
//           throw Exception('Failed to create album.');
//         }
//       }
//
//       Future<LoginRegisterResponse> response;
//       response = createAlbum();
//       FutureBuilder<LoginRegisterResponse>(
//         future: response,
//         builder: (context, snapshot) {
//           if (snapshot.hasData) {
//             return Text("hello");
//             // return Text(snapshot.data.title);
//           } else if (snapshot.hasError) {
//             return Text("${snapshot.error}");
//           }
//
//           return CircularProgressIndicator();
//         },
//       );
//
//       // LoginBloc request = LoginBloc();
//       // LoginRegisterResponse response = await request.tryLogin(requestBody);
//       // print(response);
//       // return response;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var screenSize = MediaQuery
//         .of(context)
//         .size;
//     return Scaffold(
//       body: GestureDetector(
//         onTap: () {
//           closeKyboard();
//         },
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               SizedBox(
//                 height: screenSize.height / 10,
//               ),
//               logo(),
//               SizedBox(height: screenSize.height / 12),
//               appLines(),
//               SizedBox(height: screenSize.height / 28),
//               phoneNo(),
//               SizedBox(
//                 height: screenSize.height / 50,
//               ),
//               passwrd(),
//               SizedBox(
//                 height: screenSize.height / 120,
//               ),
//               frgtpss(),
//               SizedBox(
//                 height: screenSize.height / 28,
//               ),
//               login(),
//               SizedBox(
//                 height: screenSize.height / 50,
//               ),
//               createAcc(),
//               SizedBox(
//                 height: screenSize.height / 20,
//               ),
//               orLogin(),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget logo() {
//     return Container(
//       width: double.infinity,
//       height: 50,
//       child: Image.asset("logo.png"),
//     );
//   }
//
//   Widget appLines() {
//     return Column(
//       children: [
//         Container(
//           alignment: Alignment.centerLeft,
//           margin: EdgeInsets.symmetric(horizontal: 20),
//           child: Text(
//             "Welcome,",
//             style: TextStyle(
//               fontWeight: FontWeight.bold,
//               fontSize: 18,
//             ),
//           ),
//         ),
//         Container(
//           alignment: Alignment.centerLeft,
//           margin: EdgeInsets.symmetric(horizontal: 20),
//           child: Text(
//             "You  need to login to continue",
//             style: TextStyle(fontSize: 14, color: Colors.black),
//           ),
//         ),
//       ],
//     );
//   }
//
//   Widget phoneNo() {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 20),
//       width: double.infinity,
//       height: 40,
//       decoration: BoxDecoration(
//           color: Colors.black12,
//           borderRadius: BorderRadius.all(Radius.circular(5))),
//       child: Row(
//         children: [
//           SizedBox(width: 2),
//           Container(
//             child: CountryPicker(
//               showName: false,
//               showDialingCode: true,
//               onChanged: (Country country) {
//                 setState(() {
//                   countryCode = country;
//                 });
//               },
//               selectedCountry: countryCode,
//             ),
//           ),
//           SizedBox(width: 10),
//           Icon(
//             Icons.phone,
//             size: 18,
//             color: Colors.grey,
//           ),
//           SizedBox(
//             width: 150,
//             child: TextField(
//               textAlignVertical: TextAlignVertical.center,
//               focusNode: phone,
//               controller: _phoneController,
//               keyboardType: TextInputType.phone,
//               style: TextStyle(fontFamily: 'Gothic', fontSize: 13),
//               decoration: InputDecoration(
//                 contentPadding: EdgeInsets.all(13),
//                 border: InputBorder.none,
//                 hintText: "Mobile Number ",
//                 fillColor: Colors.grey,
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget passwrd() {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 20),
//       width: double.infinity,
//       height: 40,
//       decoration: BoxDecoration(
//           color: Colors.black12,
//           borderRadius: BorderRadius.all(Radius.circular(5))),
//       child: Row(
//         children: [
//           Padding(
//             padding: const EdgeInsets.only(left: 8.0),
//             child: Icon(
//               Icons.lock,
//               size: 20,
//               color: Colors.grey,
//             ),
//           ),
//           Expanded(
//             flex: 1,
//             child: TextField(
//               textAlignVertical: TextAlignVertical.center,
//               obscureText: obscureText,
//               focusNode: pass,
//               controller: _psswrdController,
//               style: TextStyle(fontFamily: 'Gothic', fontSize: 13),
//               decoration: InputDecoration(
//                 contentPadding: EdgeInsets.all(14.5),
//                 border: InputBorder.none,
//                 hintText: "Password ",
//                 fillColor: Colors.grey,
//               ),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(right: 8.0),
//             child: InkWell(
//               onTap: () {
//                 setState(() {
//                   if (obscureText == true) {
//                     eye = Icons.visibility;
//                     obscureText = false;
//                   } else if (obscureText == false) {
//                     eye = Icons.visibility_off;
//                     obscureText = true;
//                   }
//                 });
//               },
//               child: Icon(
//                 eye,
//                 color: Colors.grey,
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget frgtpss() {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 20),
//       alignment: Alignment.topLeft,
//       child: Text(
//         "Forget Password?",
//         style: TextStyle(fontSize: 14),
//       ),
//     );
//   }
//
//   Widget login() {
//     return InkWell(
//       onTap: () {
//         onClickLogin();
//       },
//       child: Container(
//         margin: EdgeInsets.symmetric(horizontal: 20),
//         width: double.infinity,
//         height: 40,
//         decoration: BoxDecoration(
//             color: Colors.orangeAccent,
//             borderRadius: BorderRadius.all(Radius.circular(5))),
//         child: Text(
//           "LOGIN",
//           style: TextStyle(color: Colors.white, fontSize: 14),
//           textAlign: TextAlign.center,
//         ),
//         padding: EdgeInsets.symmetric(vertical: 10),
//       ),
//     );
//   }
//
//   Widget createAcc() {
//     return Container(
//       margin: EdgeInsets.symmetric(horizontal: 20),
//       width: double.infinity,
//       height: 40,
//       decoration: BoxDecoration(
//           color: Colors.white,
//           border: Border.all(),
//           borderRadius: BorderRadius.all(Radius.circular(5))),
//       child: Text(
//         "CREATE ACCOUNT",
//         textAlign: TextAlign.center,
//       ),
//       padding: EdgeInsets.symmetric(vertical: 11),
//     );
//   }
//
//   Widget orLogin() {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             line(),
//
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 5),
//               child: Text(
//                 "OR LOGIN WITH",
//                 style: TextStyle(fontSize: 10, color: Colors.grey),
//               ),
//             ),
//             // SizedBox(width: 5),
//             line(),
//           ],
//         ),
//         SizedBox(height: 25),
//         Container(
//           padding: EdgeInsets.symmetric(horizontal: 130),
//           height: 25,
//           width: double.infinity,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               Image.asset("fbook.png", fit: BoxFit.fill),
//               Image.asset("googles.png", fit: BoxFit.fill),
//             ],
//           ),
//         )
//       ],
//     );
//   }
//
//   Widget line() {
//     return Container(
//       width: 80,
//       height: 1,
//       color: Colors.grey,
//     );
//   }
//
//   void _showCenterFlash({
//     FlashPosition position,
//     FlashStyle style,
//     Alignment alignment,
//     String msg,
//   }) {
//     showFlash(
//       context: context,
//       duration: Duration(seconds: 2),
//       builder: (_, controller) {
//         return Flash(
//           controller: controller,
//           backgroundColor: Colors.black54,
//           borderRadius: BorderRadius.circular(10.0),
//           position: position,
//           style: style,
//           alignment: alignment,
//           enableDrag: false,
//           onTap: () => controller.dismiss(),
//           child: Padding(
//             padding: const EdgeInsets.all(12.0),
//             child: DefaultTextStyle(
//               style: TextStyle(
//                   color: Colors.white, fontSize: 16, fontFamily: 'Gothic'),
//               child: Text(
//                 msg,
//               ),
//             ),
//           ),
//           margin: EdgeInsets.only(bottom: 30),
//         );
//       },
//     ).then((_) {
//       if (_ != null) {
//         _showMessage(_.toString());
//       }
//     });
//   }
//
//   void _showMessage(String message) {
//     if (!mounted) return;
//     showFlash(
//         context: context,
//         duration: Duration(milliseconds: 700),
//         builder: (_, controller) {
//           return Flash(
//             controller: controller,
//             position: FlashPosition.top,
//             style: FlashStyle.grounded,
//             child: FlashBar(
//               icon: Icon(
//                 Icons.face,
//                 size: 36.0,
//                 color: Colors.black,
//               ),
//               message: Text(message),
//             ),
//           );
//         });
//   }
//
//   closeKyboard() {
//     FocusScope.of(context).requestFocus(new FocusNode());
//   }
// }
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'package:simple_api_hit/new_api_hit/model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HIT API',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'API HIT FOR OTP'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var phoneController = TextEditingController();
  FocusNode phn = FocusNode();

  Response responseOtp;
  Response responsePhoneNo;

  onClickOtpVerify() async {
    String phoneNo = phoneController.text;
    Map<String, String> body = {
      "phone_number": phoneNo,
      "otp": responseOtp.getData[0].otp.toString(),
    };
    final response1 = await http.put(
        Uri.parse("http://3.143.71.160:3008/customer/otp_verify"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(body));
    if (response1.body != null) {
      responsePhoneNo = Response.fromJson(json.decode(response1.body));
      return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("Alert Dialog Box"),
          content: Text("${responseOtp.getData[0].phoneNo}"),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text("okay"),
            ),
          ],
        ),
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Text("REGISTRATION PROCESS",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            SizedBox(height: 50),
            phoneField(context),
            Register(context),
          ],
        ));
  }

  Widget phoneField(BuildContext context) {
    var screening = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(border: Border.all()),
      width: screening.width/1.5,
      height: 40,
      child: TextField(
        textAlignVertical: TextAlignVertical.bottom,
        focusNode: phn,
        controller: phoneController,
        keyboardType: TextInputType.phone,
        style: TextStyle(
          fontFamily: 'Gothic',
          fontSize: 13,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(13),
          border: InputBorder.none,
          hintText: "Phone Number  ",
          fillColor: Colors.grey,
        ),
      ),
    );
  }

  Widget Register(BuildContext context) {
    //String phoneNo = phoneController.text;

    return InkWell(
      onTap: () async {
        Map<String, String> body = {
          "phone_number": phoneController.text,
          "device_token":
              "fQJprSCMQVeYQDrA9FcYJE:APA91bEuHUm8sh1lfu9nFSadY6pxQtLy7Ge9hjm2kNvGXak4ZOvwH8_JrC0fx2wbqaojQeCAUF0ouluhcdQGIMT5syV3VQTkGku_Ok4yh3kQ_0pihBAMdQOtfkZC3Dcsf15tL4i4Ax6q",
          "country_code": "+91",
          "device_type": 'android',
          "app_version": '0',
        };

        final response = await http.post(
            Uri.parse('http://3.143.71.160:3008/customer/register'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: json.encode(body));
        // if (response.statusCode == 200) {
        //
        //   return responseOtp;
        // } else {
        //   throw Exception('Failed to create album.');
        // }
        if (response.body != null) {
          responseOtp = Response.fromJson(json.decode(response.body));
          return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              title: Text("verification number "),
              content: Text("${responseOtp.getData[0].otp}"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Future.delayed(Duration(seconds: 5), () {
                        Navigator.of(context).pop();
                      });
                      // Navigator.of(ctx).pop();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                            onTap: () {
                              Navigator.of(ctx).pop();
                            },
                            child: Text("done")),
                        SizedBox(
                          width: 80,
                        ),
                        InkWell(
                            onTap: () {
                              onClickOtpVerify();
                            },
                            child: Text("ok"))
                      ],
                    )),
              ],
            ),
          );
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 30),
        child: Center(
          child: Container(
            color: Colors.blue,
            width: 90,
            height: 30,
            child: Center(
                child: Text(
              "Register",
              style: TextStyle(color: Colors.white),
            )),
          ),
        ),
      ),
    );
  }
}
