import 'package:simple_api_hit/loginRepo.dart';
import 'package:simple_api_hit/loginRequest.dart';
import 'package:simple_api_hit/model.dart';

class LoginBloc {
  LoginRepo repo = LoginRepo();

  Future<LoginRegisterResponse> tryLogin(LoginRequest request) async {
    LoginRegisterResponse response = await repo.tryLogin(request);

    return response;
  }
}
