class LoginRegisterResponse {
  Data _data;
  String _profile_img;
  HandymanAccount _handymanAccount;

  LoginRegisterResponse.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson['data'] != null) {
      _data = Data(parsedJson['data']);
    }
    if (parsedJson['profile_img'] != null) {
      _profile_img = parsedJson['profile_img'];
    }
    if (parsedJson['handymanAccount'] != null) {
      _handymanAccount = HandymanAccount(parsedJson['handymanAccount']);
    }
  }

  Data get data => _data;

  String get profile_img => _profile_img;

  HandymanAccount get handymanAccount => _handymanAccount;
}

class Data {
  int _id;
  String _full_name;
  String _email;
  String _phone_number;
  int _is_verified;
  int _is_phone_verified;
  String _access_token;
  int _is_document_uploaded;
  int _is_documented_verified;
  String _experience;
  int _category_id;
  String _address;
  String _city;
  String _state;
  String _country;
  int _is_bank_account;
  String _country_code;

  Data(parsedJson) {
    _id = parsedJson['id'];
    _full_name = parsedJson['full_name'];
    _email = parsedJson['email'];
    _phone_number = parsedJson['phone_number'];
    _is_verified = parsedJson['is_verified'];
    _is_phone_verified = parsedJson['is_phone_verified'];
    _access_token = parsedJson['access_token'];
    _is_document_uploaded = parsedJson['is_document_uploaded'];
    _is_documented_verified = parsedJson['is_document_verified'];
    _experience = parsedJson['experience'];
    _category_id = parsedJson['category_id'];
    _address = parsedJson['address'];
    _city = parsedJson['city'];
    _state = parsedJson['state'];
    _country = parsedJson['country'];
    _is_bank_account = parsedJson['is_bank_account'];
    _country_code = parsedJson['country_code'];
  }

  int get id => _id;

  String get full_name => _full_name;

  String get email => _email;

  String get phone_number => _phone_number;

  int get is_verified => _is_verified;

  int get is_phone_verified => _is_phone_verified;

  String get access_token => _access_token;

  int get is_document_uploaded => _is_document_uploaded;

  int get is_documented_verified => _is_documented_verified;

  String get experience => _experience;

  int get category_id => _category_id;

  String get address => _address;

  String get city => _city;

  String get state => _state;

  String get country => _country;

  int get is_bank_account => _is_bank_account;

  set is_bank_account(int value) {
    _is_bank_account = value;
  }

  String get country_code => _country_code;
}

class HandymanAccount {
  String _handyman_account_id;
  int _status;
  int _type;
  String _bank_name;
  String _full_name;
  String _account_no;
  String _abn;
  String _isGst;

  HandymanAccount(parsedJson) {
    _handyman_account_id = parsedJson['handyman_account_id'];
    _status = parsedJson['status'];
    _type = parsedJson['type'];
    _bank_name = parsedJson['bank_name'];
    _full_name = parsedJson['full_name'];
    _account_no = parsedJson['account_no'];
    _abn = parsedJson['abn'];
    _isGst = parsedJson['is_gst'].toString();
  }

  String get handyman_account_id => _handyman_account_id;

  int get status => _status;

  int get type => _type;

  String get full_name => _full_name;

  String get account_no => _account_no;

  String get abn => _abn;

  String get bank_name => _bank_name;

  String get isGst => _isGst;
}
