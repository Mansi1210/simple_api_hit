class LoginRequest {
  String loginToken = "e639c68cb28da92674c0efc0561614a9";
  String phoneNo;
  String password;
  String login_type = '0';
  String login_or_signup = '0';
  String device_token;
  String country_code;

  LoginRequest(
      {this.phoneNo, this.password, this.device_token, this.country_code});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["loginToken"] = loginToken;
    map['country_code'] = country_code;
    map["phone_number"] = phoneNo;
    map["password"] = password;
    map["login_type"] = login_type;
    map["login_or_signup"] = login_or_signup;
    map['device_token'] = device_token;

    return map;
  }
}
