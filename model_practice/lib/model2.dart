// class LoginResgisterResponse {
//   int statusCode;
//   String message;
//   List<Data> data;
//
//   LoginResgisterResponse({this.statusCode, this.message, this.data});
//
//   LoginResgisterResponse.fromJson(Map<dynamic, dynamic> json) {
//     statusCode = json['statusCode'];
//     message = json['message'];
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<dynamic, dynamic>();
//     data['statusCode'] = this.statusCode;
//     data['message'] = this.message;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Data {
//   int userId;
//   String name;
//   String countryCode;
//   String phoneNumber;
//   int status;
//   int isDeleted;
//   int otp;
//   String accessToken;
//   String deviceType;
//   String deviceToken;
//   Null profilePic;
//   String lastSeen;
//   Null statusMsg;
//   String online;
//   Null longitude;
//   Null latitude;
//   String createdAt;
//
//   Data(
//       {this.userId,
//         this.name,
//         this.countryCode,
//         this.phoneNumber,
//         this.status,
//         this.isDeleted,
//         this.otp,
//         this.accessToken,
//         this.deviceType,
//         this.deviceToken,
//         this.profilePic,
//         this.lastSeen,
//         this.statusMsg,
//         this.online,
//         this.longitude,
//         this.latitude,
//         this.createdAt});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     userId = json['user_id'];
//     name = json['name'];
//     countryCode = json['country_code'];
//     phoneNumber = json['phone_number'];
//     status = json['status'];
//     isDeleted = json['is_deleted'];
//     otp = json['otp'];
//     accessToken = json['access_token'];
//     deviceType = json['device_type'];
//     deviceToken = json['device_token'];
//     profilePic = json['profile_pic'];
//     lastSeen = json['last_seen'];
//     statusMsg = json['status_msg'];
//     online = json['online'];
//     longitude = json['longitude'];
//     latitude = json['latitude'];
//     createdAt = json['created_at'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['user_id'] = this.userId;
//     data['name'] = this.name;
//     data['country_code'] = this.countryCode;
//     data['phone_number'] = this.phoneNumber;
//     data['status'] = this.status;
//     data['is_deleted'] = this.isDeleted;
//     data['otp'] = this.otp;
//     data['access_token'] = this.accessToken;
//     data['device_type'] = this.deviceType;
//     data['device_token'] = this.deviceToken;
//     data['profile_pic'] = this.profilePic;
//     data['last_seen'] = this.lastSeen;
//     data['status_msg'] = this.statusMsg;
//     data['online'] = this.online;
//     data['longitude'] = this.longitude;
//     data['latitude'] = this.latitude;
//     data['created_at'] = this.createdAt;
//     return data;
//   }
// }