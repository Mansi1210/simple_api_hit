

class Response {
  List <Profile> _getData=[];

  int _statusCode;

  int get statusCode => _statusCode;
  String _message;
  List<Profile> get getData => _getData;


  Response.fromJson(Map<dynamic, dynamic> json) {
    _statusCode = json["statusCode"];
    _message = json["message"];
     List<Profile> list1=[];
    for(int i=0;i<json["data"].length;i++){
      Profile data = Profile(json["data"][i]);
      list1.add(data);
    }
    _getData=list1;
// _getData  = json["data"];
    // _getData = Profile.fromJson(json ["data"]);
  // _getData = Data.fromJson(json["data" as Map<String,dynamic>])  ;
  }

  String get message => _message;
}

 class Profile {
   int _userId;
   String _name;
   String _country_code;
   String _phoneNo;
   int _status;
   int _isDeleted;
   int _otp;
   String _accessToken;
   String _deviceType;
   String _devicetoken;
   String _profilePic;
   String _lastSeen;
   String _statusMsg;
  String _online;
  double _longitude;
   double _latitude;
  String _createdAt;

   Profile(Map<String, dynamic> json) {
     _userId = json["user_id"];
     _name = json["name"];
     _country_code = json["country_code"];
     _phoneNo = json["phone_number"];
     _status = json["status"];
     _isDeleted = json["is_deleted"];
     _otp = json["otp"];
     _accessToken = json["access_token"];
     _deviceType = json["device_type"];
     _devicetoken = json["device_token"];
     _profilePic = json["profile_pic"];
     _lastSeen = json["last_seen"];
     _statusMsg = json["status_msg"];
     _online = json["online"];
     _longitude = json["longitude"];
     _latitude = json["latitude"];
     _createdAt = json["created_at"];
   }

   String get createdAt => _createdAt;

   double get latitude => _latitude;

   double get longitude => _longitude;

   String get online => _online;

   String get statusMsg => _statusMsg;

  String get lastSeen => _lastSeen;

   String get profilePic => _profilePic;

   String get devicetoken => _devicetoken;

   String get deviceType => _deviceType;

   String get accessToken => _accessToken;

   int get otp => _otp;

   int get isDeleted => _isDeleted;

   int get status => _status;

   String get phoneNo => _phoneNo;

  String get country_code => _country_code;

   String get name => _name;

   int get userId => _userId;
 }
