import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dataBloc.dart';
import 'json.dart';
import 'model.dart';


class Streams extends StatefulWidget {

  @override
  _StreamsState createState() => _StreamsState();
}

class _StreamsState extends State<Streams> {
  DataBloc bloc = DataBloc();

  @override
  Widget build(BuildContext context) {
    bloc.getData() ;
    return StreamBuilder(
        stream: bloc.dataController.stream,
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return Text(snapshot.data.message);
          } else {
            return CircularProgressIndicator();
          }
        });
  }

}
