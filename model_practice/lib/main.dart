import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:model_practice/json.dart';
import 'package:model_practice/model.dart';
import 'package:model_practice/stream.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          children: [
            // Streams(),
            Login(context),
          ],
        ));
  }
}

Widget Login(BuildContext context) {
  return InkWell(
    onTap: () async {
      Map<String, String> body = {
        "phone_number": "9087654312",
        "device_token":
            "fQJprSCMQVeYQDrA9FcYJE:APA91bEuHUm8sh1lfu9nFSadY6pxQtLy7Ge9hjm2kNvGXak4ZOvwH8_JrC0fx2wbqaojQeCAUF0ouluhcdQGIMT5syV3VQTkGku_Ok4yh3kQ_0pihBAMdQOtfkZC3Dcsf15tL4i4Ax6q",
        "country_code": "+91",
        "device_type": 'android',
        "app_version": '0',
      };

      // Future<Response> createAlbum() async {
      final response = await http.post(
          Uri.parse('http://3.143.71.160:3008/customer/register'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: json.encode(body));
      if(response.body !=null){
        return getResult(context);
      }
   // if (response.statusCode == 200) {
      //   return Response.fromJson(json.decode(response.body));
      //           // return getResult();
      // }
      // else {
      //
      //   throw Exception('Failed to create album.');
      // }
    },
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal:18.0,vertical: 80),
      child: Center(
        child: Container(
          color: Colors.blue,
          width: 50,
          height: 30,
          child: Center(child: Text("login",style: TextStyle(color: Colors.white),)),
        ),
      ),
    ),
  );
}

getResult(BuildContext context) {
  Response result = Response.fromJson(file);
  // print(result.getData[0].otp);
  // return result.getData[0].otp;
  return showDialog(
    context: context,
    builder: (ctx) => AlertDialog(
      title: Text("Alert Dialog Box"),
      content: Text("${result.getData[0].otp}"),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(ctx).pop();
          },
          child: Text("okay"),
        ),
      ],
    ),
  );
}
